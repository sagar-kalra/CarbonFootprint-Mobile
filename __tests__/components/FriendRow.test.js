import FriendRow from '../../app/components/FriendRow';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('FriendRow test suite', () => {
    test('renders without error', () => {
        let expectedProps = {
            data: {},
            text: 'john',
            iconName: ['someIcon']
        };

        const wrapper = setup(FriendRow, expectedProps);
        let rootComponent = findByAttr(wrapper, 'friendrow-component');

        expect(rootComponent.length).toBe(1);
    });

    test('Rendering without error, only the first icon', () => {
        let expectedProps = {
            data: {},
            text: 'john',
            iconName: ['someIcon']
        };

        const wrapper = setup(FriendRow, expectedProps);
        let rootComponent = findByAttr(wrapper, 'friendrow-component');
        let firstIcon = findByAttr(wrapper, 'first-icon');
        let secondIcon = findByAttr(wrapper, 'second-icon');

        expect(rootComponent.length).toBe(1);
        expect(firstIcon.length).toBe(1);
        expect(secondIcon.length).toBe(0);
    });

    test('Simulate clicking on first-icon and second-icon', () => {
        let expectedProps = {
            data: {},
            text: 'john',
            iconName: ['someIcon', 'secondIcon'],
            link: jest.fn(),
            reject: jest.fn()
        };

        const wrapper = setup(FriendRow, expectedProps);
        let firstIcon = findByAttr(wrapper, 'first-icon');
        let secondIcon = findByAttr(wrapper, 'second-icon');

        firstIcon.simulate('press');
        secondIcon.simulate('press');
        // Called once
        expect(expectedProps.link.mock.calls).toHaveLength(1);
        expect(expectedProps.reject.mock.calls).toHaveLength(1);
    });

    test('Rendering without error, first and second icon', () => {
        let expectedProps = {
            data: {},
            text: 'john',
            iconName: ['someIcon', 'anotherIcon']
        };

        const wrapper = setup(FriendRow, expectedProps);
        let rootComponent = findByAttr(wrapper, 'friendrow-component');
        let firstIcon = findByAttr(wrapper, 'first-icon');
        let secondIcon = findByAttr(wrapper, 'second-icon');

        expect(rootComponent.length).toBe(1);
        expect(firstIcon.length).toBe(1);
        expect(secondIcon.length).toBe(1);
    });

    test('Checking prop types', () => {
        let expectedProps = {
            data: {},
            text: 'john',
            iconName: ['someIcon']
        };

        const propError = checkProps(expectedProps);

        expect(propError).toBeUndefined();
    });
});
