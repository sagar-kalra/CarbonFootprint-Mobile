import ModTextInput from '../../app/components/ModTextInput';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('ModTextInput test suite', () => {
    let wrapper = null;
    let expectedProps = {
        stateKey: 'something'
    };

    beforeEach(() => {
        wrapper = setup(ModTextInput, expectedProps);
    });

    test('renders without error', () => {
        let rootComponent = findByAttr(wrapper, 'modtextinput-component');

        expect(rootComponent.length).toBe(1);
    });

    test('Check prop types', () => {
        let propError = checkProps(ModTextInput, expectedProps);

        // propError is undefined if required props are passed
        expect(propError).toBeUndefined();
    });
});
