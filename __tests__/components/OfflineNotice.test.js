import OfflineNotice from '../../app/components/OfflineNotice';
import { shallow } from 'enzyme';
import React from 'react';
import checkPropTypes from 'check-prop-types';

const setup = (Component, props = {}, initialState = null) => {
    let wrapper = shallow(<Component {...props} />);
    if (initialState) wrapper.setState(state);

    return wrapper;
};

/**
 * Factory function to return the ShallowWrapper with the root as node having testID = val.
 * @param {ShallowWrapper} wrapper - Given ShallowWrapper to search within.
 * @param {string} val - Value of testID attribute for search.
 * @return {ShallowWrapper} - Return ShallowWrapper containing node(s) with the given testID attribute
 */
const findByAttr = (wrapper, val) => {
    return wrapper.find(`[testID="${val}"]`);
};

/**
 * Checks if the required props are being passed to a component.
 * @param {React.Component} component
 * @param {object} conformingProps
 */
const checkProps = (component, conformingProps) => {
    const propError = checkPropTypes(component.propTypes, conformingProps, 'prop', component.name);
    return propError;
};

describe('OfflineNotice test suite', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = setup(OfflineNotice);
    });

    test('renders without error', () => {
        let rootComponent = findByAttr(wrapper, 'offlinenotice-component');
        expect(rootComponent.length).toBe(0);
    });
});
