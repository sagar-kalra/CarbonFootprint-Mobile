import ProfileHeader from '../../app/components/ProfileHeader';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('ProfileHeader test suite case', () => {
    test('renders without error', () => {
        const wrapper = setup(ProfileHeader);
        let rootComponent = findByAttr(wrapper, 'profileheader-component');
        let icon = findByAttr(wrapper, 'icon');

        expect(rootComponent.length).toBe(1);
        expect(icon.length).toBe(0);
    });
});
