import TodayTab from '../../app/components/TodayTab';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';
import ActivityHistoryStorage from '../../app/actions/ActivityHistoryStorage';

describe('TodayTab test suite', () => {
    let wrapper = null;
    ActivityHistoryStorage.createDB = jest.fn(() => console.log('createDB called'));
    ActivityHistoryStorage.getTotalData = jest.fn(() => {
        return {
            co2Emitted: 5,
            co2Saved: 6,
            co2WalkSaved: 7,
            co2RunSaved: 8,
            co2CycleSaved: 9,
            co2VehicleEmitted: 10,
            dist: 5,
            distWalk: 5,
            distRun: 5,
            distCycle: 5,
            distVehicle: 5
        };
    });

    beforeEach(() => {
        wrapper = setup(TodayTab);
    });

    test('renders without error', () => {
        let rootComponent = findByAttr(wrapper, 'todaytab-component');

        expect(rootComponent.length).toBe(1);
    });

    test('Check state', () => {
        let co2Saved = wrapper.state().co2Saved;

        // propError is undefined if required props are passed
        expect(co2Saved).toBe(6);
    });
});
