import WarningTextAndIcon from '../../app/components/WarningTextAndIcon';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('WarningTextAndIcon test suite case', () => {
    test('renders without error', () => {
        let expectedProps = {
            iconName: 'close'
        };
        const wrapper = setup(WarningTextAndIcon, expectedProps);
        let rootComponent = findByAttr(wrapper, 'warningtextandicon-component');
        expect(rootComponent.length).toBe(1);
    });
});
