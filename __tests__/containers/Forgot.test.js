import Forgot from '../../app/containers/Forgot';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('Forgot test suite', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = setup(Forgot, undefined, undefined, true);
    });

    test('renders without error', () => {
        let rootComponent = findByAttr(wrapper, 'forgot-container');

        expect(rootComponent.length).toBe(1);
    });
});
