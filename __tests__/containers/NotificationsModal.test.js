import NotificationsModal from '../../app/containers/NotificationsModal';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('NotificationsModal test suite', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = setup(NotificationsModal, undefined, undefined, true);
    });

    test('renders without error', () => {
        let rootComponent = findByAttr(wrapper, 'notificationsmodal-container');

        expect(rootComponent.length).toBe(1);
    });
});
