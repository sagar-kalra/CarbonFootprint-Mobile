import StaticMap from '../../app/containers/StaticMap';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('StaticMap test suite', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = setup(StaticMap, undefined, undefined, true);
    });

    test('renders without error', () => {
        let rootComponent = findByAttr(wrapper, 'staticmap-container');

        expect(rootComponent.length).toBe(1);
    });
});
