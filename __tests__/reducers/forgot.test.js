// import ForgotReducer from '../../app/reducers/forgot';
// import { REQUEST_FORGOT, RECEIVE_FORGOT } from '../../app/actions/AuthAction';
const stateMerge = (EXPECTED_INITIAL_STATE = {}, obj = {}, additionalProperties = {}) => {
    let mergedObject = {
        ...EXPECTED_INITIAL_STATE,
        ...obj,
        ...additionalProperties
    };

    // can also use "key" in mergedObject -> it will go down the proptotype chain though.
    if (mergedObject.hasOwnProperty('type')) delete mergedObject.type;

    return mergedObject;
};

describe('testing direction reducer suite', () => {
    const EXPECTED_INITIAL_STATE = {
        isFetching: false,
        message: ''
    };

    test('initial/default state', () => {
        // switching over undefined
        // let actualState = ForgotReducer(undefined, {});
        // expect(actualState).toEqual(EXPECTED_INITIAL_STATE);
    });

    // test('REQUEST_FORGOT type passed', () => {
    //     let action = {
    //         type: REQUEST_FORGOT
    //     };
    //     let actualState = ForgotReducer(undefined, action);

    //     let expectedState = stateMerge(EXPECTED_INITIAL_STATE, action, {
    //         isFetching: true,
    //         message: ''
    //     });

    //     expect(actualState).toEqual(expectedState);
    // });

    // test('RECEIVE_FORGOT type passed', () => {
    //     let action = {
    //         type: RECEIVE_FORGOT,
    //         message: 'hello'
    //     };
    //     let actualState = ForgotReducer(undefined, action);

    //     let expectedState = stateMerge(EXPECTED_INITIAL_STATE, action, { isFetching: false });

    //     expect(actualState).toEqual(expectedState);
    // });
});
