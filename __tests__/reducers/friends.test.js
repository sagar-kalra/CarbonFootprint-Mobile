// import FriendsReducer from '../../app/reducers/friends';
// import { REQUEST_FRIENDS, RECEIVE_FRIENDS, RECEIVE_ERROR } from '../../app/actions/FriendsAction';

const stateMerge = (EXPECTED_INITIAL_STATE = {}, obj = {}, additionalProperties = {}) => {
    let mergedObject = {
        ...EXPECTED_INITIAL_STATE,
        ...obj,
        ...additionalProperties
    };

    // can also use "key" in mergedObject -> it will go down the proptotype chain though.
    if (mergedObject.hasOwnProperty('type')) delete mergedObject.type;

    return mergedObject;
};

describe('testing direction reducer suite', () => {
    const EXPECTED_INITIAL_STATE = {
        isFetching: false,
        list: null,
        error: ''
    };

    test('initial/default state', () => {
        // switching over undefined
        // let actualState = FriendsReducer(undefined, {});
        // expect(actualState).toEqual(EXPECTED_INITIAL_STATE);
    });

    // test('REQUEST_FRIENDS type passed', () => {
    //     let action = {
    //         type: REQUEST_FRIENDS
    //     };
    //     let actualState = FriendsReducer(undefined, action);

    //     let expectedState = stateMerge(EXPECTED_INITIAL_STATE, action, { isFetching: true });

    //     expect(actualState).toEqual(expectedState);
    // });

    // test('RECEIVE_FRIENDS type passed', () => {
    //     let action = {
    //         type: RECEIVE_FRIENDS,
    //         list: 'some type of list'
    //     };
    //     let actualState = FriendsReducer(undefined, action);

    //     let expectedState = stateMerge(EXPECTED_INITIAL_STATE, action, {
    //         isFetching: false,
    //         error: ''
    //     });

    //     expect(actualState).toEqual(expectedState);
    // });

    // test('RECEIVE_ERROR type passed', () => {
    //     let action = {
    //         type: RECEIVE_ERROR,
    //         error: 'some type of error'
    //     };
    //     let actualState = FriendsReducer(undefined, action);

    //     let expectedState = stateMerge(EXPECTED_INITIAL_STATE, action, {
    //         isFetching: false,
    //         list: null
    //     });

    //     expect(actualState).toEqual(expectedState);
    // });
});
