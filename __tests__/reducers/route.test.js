import Reducer from '../../app/reducers/route';
import { ActionConst } from 'react-native-router-flux';

const stateMerge = (EXPECTED_INITIAL_STATE = {}, obj = {}, additionalProperties = {}) => {
    let mergedObject = {
        ...EXPECTED_INITIAL_STATE,
        ...obj,
        ...additionalProperties
    };

    // can also use "key" in mergedObject -> it will go down the proptotype chain though.
    if (mergedObject.hasOwnProperty('type')) delete mergedObject.type;

    return mergedObject;
};

describe('Intro test suite case', () => {
    const EXPECTED_INITIAL_STATE = {
        scene: {}
    };

    test('initial/default state', () => {
        // switching over undefined
        let actualState = Reducer(undefined, {});

        expect(actualState).toEqual(EXPECTED_INITIAL_STATE);
    });

    test('ActionConst.FOCUS type passed', () => {
        let action = {
            type: ActionConst.FOCUS,
            scene: {
                one: 'screen'
            }
        };
        let actualState = Reducer(undefined, action);

        let expectedState = stateMerge(EXPECTED_INITIAL_STATE, action);

        expect(actualState).toEqual(expectedState);
    });
});
