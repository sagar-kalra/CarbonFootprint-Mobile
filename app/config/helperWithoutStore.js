import LocationServicesDialogBox from 'react-native-android-location-services-dialog-box';
import { Platform, BackHandler } from 'react-native';

export function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    hours = hours < 10 ? '0' + hours : hours;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var time = hours + ':' + minutes + ' ' + ampm;
    return time;
}

/**
 * check gps at run time to enable or disable
 * @return {Alert} gps enabled or disabled
 */
export function checkGPS() {
    LocationServicesDialogBox.checkLocationServicesIsEnabled({
        message: '<h2>Enable GPS</h2>This app wants to use GPS. Please enable GPS.',
        ok: 'YES',
        cancel: 'NO',
        enableHighAccuracy: true, // true => GPS AND NETWORK PROVIDER, false => ONLY GPS PROVIDER
        showDialog: true // false => Opens the Location access page directly
    })
        .then(
            function(success) {
                //return true;
            }.bind(this)
        )
        .catch(error => {
            //return false;
            //console.log(error.message);
        });

    BackHandler.addEventListener('hardwareBackPress', () => {
        LocationServicesDialogBox.forceCloseDialog();
    });
}

/*
 * Format email(replacing '.' to ','), so that it can be used as a primary key.
 * Firebase doesn't allow '.'(dot) to be used in the key text
 */

export function formatEmail(userEmail) {
    return userEmail.replace(/\./g, ',');
}
